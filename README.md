# Projekti: Kisatulosten seuranta

Reactilla (MUI + ApexChart) toteutettu käyttöliittymä, joka hakee kisakohtaiset API-tiedot (https://discgolfmetrix.com/?u=rule&ID=37) ulkopuolisesta palvelusta. Haetut tiedot säilötään JSON serveriin, josta data voidaan hakea helpommin käytettäväksi kokonaisuudeksi.

Appin käyttötarkoitus on tukea (toistaiseksi vain yhtä) liigapohjaista kisatapahtumaa, automatisoimalla pisteenlaskun ja sijoitustaulukon päivityksen useamman osakilpailun tietojen mukaan. APIn ollessa vielä kehitysasteella, automaattista seurantaa varten tarvitaan käyttäjää lisäämään jokainen kisa (id).

## Asennus

-   npm install

*   JSON serverin käynnistys komennolla "npx json-server --port=3001 --watch ./src/Server/db.json"

## Sivut

-   AdminPage - Kisatietojen lisääminen/poistaminen kisaID numeron mukaan. Yleiskuva kaikista tietokannan sisältämistä tiedoista.
-   HomePage - Yleiskuva lisätyistä kisoista.

### Roadmap (tärkeysjärjestyksessä)

-   Kisakohtainen pisteenlasku kokonaistulosten perusteella. Top-10 pisteytetään.
-   Lopulliseen pisteenlaskuun lasketaan vain pelaajan X parasta sijoitusta.
-   Sarjataulukot, luokkien erottelu, sijoitus jokaisen kisan yhteenlasketun pisteiden perusteella.
-   Pelaajan suoritusten tilastointi kisoissa ja niiden esittäminen ApexChartia hyödyntämällä.
-   Radan vaikeuden tilastointi (ApexChart Heatmap).
-   Yleistä tilastointia osallistujamääristä/aktiivisuudesta.
-   Pelaajien välinen vertailu.
-   Palkintopotin jakolasku/t(?) ylläpitäjän sivulle.
-   Potentiaalinen sijoitusarviointi edellisten tulosten perusteella.
-   NodeJS+express+SQL toteutus JSON Serverin tilalle.
-   Tuki useammalle liigalle/turnaukselle.
