import RemoveComp from "./RemoveComp";
import StoredComps from "../Server/db.json";
import { Link } from "react-router-dom";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const ShowComps = ({ setData, adminRights }) => {
  return StoredComps.Competition.map((comp) => {
    const cid = comp.id;

    return adminRights === 1 ? (
      <li key={comp.id}>
        <Link to={`/${comp.id}`}>{comp.Name}</Link>

        <IconButton
          variant="outlined"
          aria-label="delete"
          size="small"
          onClick={() => RemoveComp({ cid, StoredComps, setData })}
        >
          <DeleteIcon />
        </IconButton>
      </li>
    ) : (
      <p key={cid}>
        <Link to={`/${comp.id}`}>{comp.Name}</Link>
      </p>
    );
  });
};

export default ShowComps;
