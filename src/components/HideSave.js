import storedComps from "../Server/db.json";

const HideSave = ({ data, compID, SavetoDB }) => {
  return data === null ? (
    ""
  ) : storedComps.Competition.find((comp) => comp.id === compID) === true ? ( // WIP "undefined" Ei saa luettavaa dataa sisään
    "Kisa on jo tallennettu: "
  ) : (
    <button onClick={() => SavetoDB({ data, compID })}>Tallenna</button>
  );
};

export default HideSave;
