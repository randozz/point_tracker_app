import axios from "axios";

const SavetoDB = async ({ data, compID }) => {
  const cmp = data.Competition;

  const compObject = {
    id: Number(compID),
    Name: cmp.Name,
    Type: cmp.Type,
    TourDateStart: cmp.TourDateStart,
    TourDateEnd: cmp.TourDateEnd,
    Date: cmp.Date,
    Time: cmp.Time,
    Comment: cmp.Comment,
    CourseName: cmp.CourseName,
    CourseID: cmp.CourseID,
    MetrixMode: Number(cmp.MetrixMode),
    ShowPreviousRoundsSum: cmp.ShowPreviousRoundsSum,
    HasSubcompetitions: cmp.HasSubcompetitions,
    WeeklyHCSummary: cmp.WeeklyHCSummary,
    WeeklyHC: cmp.WeeklyHC,
    Results: cmp.Results,
    Tracks: cmp.Tracks,
  };

  await axios
    .post("http://localhost:3001/Competition", compObject)
    .then(() => {
      alert(compID + " saved succesfully!");
    })
    .catch((error) => {
      console.log("error", error.response.data);
    });
};

export default SavetoDB;
