const results = (props) => {
  return (
    <>
      Nimi: {JSON.parse(JSON.stringify(props.compData.Results[0].Name))}
      <br />
      Tulos: {JSON.parse(JSON.stringify(props.compData.Results[0].Sum))} (
      {JSON.parse(JSON.stringify(props.compData.Results[0].Diff))})
    </>
  );
};

export default results;
