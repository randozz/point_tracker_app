import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Results from "./CompResults";

const PlayerCell = ({ compData }) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ width: 1 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Pelaaja</TableCell>
            <TableCell align="center">Sijoitus</TableCell>
            <TableCell align="center">Luokka</TableCell>
            <TableCell align="center">Tulos</TableCell>
            <TableCell align="left">+/-</TableCell>
            <TableCell align="center">Pisteet</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {compData.Results.map((player) => {
            return <Results playerData={player} key={player.UserID} />;
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PlayerCell;
