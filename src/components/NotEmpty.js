const NotEmpty = ({ data }) => {
  return data === null
    ? "Lisää kisa ID"
    : JSON.stringify(data.Competition.Name);
};

export default NotEmpty;
