const FetchApiData = async ({ combinedURL, compID, setData }) => {
  const response = await fetch(combinedURL);
  const jsonData = await response.json();
  console.log(jsonData);

  const idValid = jsonData.hasOwnProperty("Errors")
    ? alert("Virheellinen ID: " + compID)
    : setData(jsonData);
  return idValid;
};

export default FetchApiData;
