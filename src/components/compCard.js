const compCard = (props) => {
  const text = (
    <>
      <p>Kisa: {props.compData.Name}</p>
      <p>ID: {props.compData.id}</p>
      <p>Päivä: {props.compData.Date}</p>
      {/*  <p>Rata: {props.compData.CourseName}</p>
      <p>Kommentti: {props.compData.Comment}</p> */}
      <hr />
      <p>Pelaajamäärä: {props.compData.Results.length}</p>
    </>
  );

  return text;
};

export default compCard;
