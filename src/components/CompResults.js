import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";

const results = (props) => {
  const PlayerClass = props.playerData.ClassName;

  const Position = props.playerData.OrderNumber;
  const PlayerName = props.playerData.Name;
  const Score = props.playerData.Sum;
  const DiffToPar = props.playerData.Diff;

  const Points =
    Position === 1
      ? "10"
      : Position === 2
      ? "9"
      : Position === 3
      ? "8"
      : Position === 4
      ? "7"
      : Position === 5
      ? "6"
      : Position === 6
      ? "5"
      : Position === 7
      ? "4"
      : Position === 8
      ? "3"
      : Position === 9
      ? "2"
      : Position === 10
      ? "1"
      : "";

  return (
    <TableRow
      key={props.playerData.UserID}
      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
    >
      <TableCell component="th" scope="row">
        {PlayerName}
      </TableCell>
      <TableCell align="center">{Position}</TableCell>
      <TableCell align="center">{PlayerClass}</TableCell>
      <TableCell align="center">{Score}</TableCell>
      <TableCell align="left">{DiffToPar}</TableCell>
      <TableCell align="center">{Points}</TableCell>
    </TableRow>
  );
};

export default results;
