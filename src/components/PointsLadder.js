import data from "../Server/db.json";

const PointsLadder = () => {
    // WIP - Haetaan kisoista top-10 sijoitukset, pisteytetään, lisätään stateen ja lasketaan yhteen
    const testi = data.Competition[3].Results.filter(
        (player) => player.OrderNumber <= 5
    ).map((filteredPlayer) => {
        return (
            <div key={filteredPlayer.Name}>
                {filteredPlayer.OrderNumber}: {filteredPlayer.Name}
            </div>
        );
    });
    console.log(data.length);
    console.log(testi.length);
    return testi;
};

export default PointsLadder;
