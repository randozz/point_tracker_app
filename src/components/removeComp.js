import axios from "axios";

const RemoveComp = ({ cid, StoredComps, setData }) => {
  axios
    .delete("http://localhost:3001/Competition/" + cid)
    .then(setData(StoredComps.Competition.filter((comp) => comp.id !== cid)))
    .then(() => alert(cid + " deleted successfully"));
};

export default RemoveComp;
