import * as React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CompCard from "./CompCard";
import PlayerCell from "./PlayerCell";

const CompDetails = () => {
  const { compid } = useParams();
  const getCompUrl = compid;
  const [compData, setCompData] = useState(null);

  const getData = () => {
    axios
      .get(`http://localhost:3001/Competition/${getCompUrl}`)
      .then((Response) => {
        console.log(Response);
        const gotData = Response.data;
        setCompData(gotData);
      });
  };

  useEffect(() => getData(), []);

  const checkIfEmpty =
    compData === null ? (
      "Loading.."
    ) : (
      <>
        <CompCard compData={compData} />
        <hr />

        {/* MUI Dense table - WIP */}
        <PlayerCell compData={compData} />
      </>
    );

  return checkIfEmpty;
};

export default CompDetails;
