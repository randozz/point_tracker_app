import "./App.css";
import { Routes, Route } from "react-router-dom";
import Header from "./routes/Header";
import Footer from "./routes/Footer";
import AdminPage from "./routes/AdminPage";
import MainPage from "./routes/MainPage";
import CompDetails from "./components/CompDetails";

function App() {
  return (
    <div className="mainBody">
      <Header />

      <Routes>
        <Route path="/AdminPage" element={<AdminPage />} />
        <Route path="/:compid" element={<CompDetails />} />
        <Route path="/" element={<MainPage />} />
        <Route path="*" element={<p>Oops! Wrong address.</p>} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
