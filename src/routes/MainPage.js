import PointsLadder from "../components/PointsLadder";
import ShowComps from "../components/ShowComps";

const MainPage = () => {
    return (
        <div className="home-body">
            <div>
                <h3>Top-5 (Ei luokkajakoa, tunnistus puuttuu kokonaan)</h3>
                <PointsLadder />
            </div>
            <hr />
            <div>
                <ShowComps />
            </div>
        </div>
    );
};

export default MainPage;
