import { useState } from "react";
import ShowComps from "../components/ShowComps";
import FetchApiData from "../components/FetchApiData";
import HideSave from "../components/HideSave";
import NotEmpty from "../components/NotEmpty";
import SavetoDB from "../components/SaveToDB";
import { Button, List, TextField } from "@mui/material";

const AdminPage = () => {
  const [compID, setCompID] = useState(null); /* Kisan ID tunniste */
  const [data, setData] = useState(null); /* API data */

  const URL = "https://discgolfmetrix.com/";
  const combinedURL = URL + "api.php?content=result&id=" + compID;

  const onChange = (e) => {
    setCompID(e.target.value);
  };

  return (
    <div className="home-body">
      <TextField type="text" size="small" onChange={onChange} label="ID..." />

      <Button
        variant="contained"
        size="medium"
        onClick={() => FetchApiData({ combinedURL, compID, setData })}
        disabled={compID === null}
        title="Hae..."
      >
        Hae..
      </Button>

      <br />
      <i>p.s. 1572829, 1572830, 1572831</i>
      <br />
      <br />
      <HideSave data={data} compID={compID} SavetoDB={SavetoDB} />
      <NotEmpty data={data} />
      <br />
      <br />
      <hr />
      <List>
        <ShowComps setData={setData} adminRights={1} />
      </List>
    </div>
  );
};

export default AdminPage;
